#
# Be sure to run `pod lib lint SOKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FDFoundation'
  s.version          = '0.0.1'
  s.summary          = '多App 通用UI组件'
  s.description      = <<-DESC
  多App 通用UI组件
                       DESC

  s.homepage         = 'https://git.nevint.com/DOMOBILE-FeiDian/FDUIKit'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'xxx' => 'xxx' }
  s.source           = { :http => 'http://gitlab.com/1995zhouJinYing/document/raw/master/FDFoundation.zip'}
  s.vendored_frameworks = "*/FDFoundation.framework"
  s.xcconfig            = { "LIBRARY_SEARCH_PATHS" => "\"$(PODS_ROOT)/FDFoundation/**\"" }
  
  end

  